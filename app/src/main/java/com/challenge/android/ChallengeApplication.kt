package com.challenge.android

import android.app.Application
import com.challenge.di.registerDatabase
import com.challenge.list.di.registerListModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

open class ChallengeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            printLogger()
            androidContext(this@ChallengeApplication)
            modules(
                registerListModule() +
                        registerDatabase()
            )
        }
    }
}