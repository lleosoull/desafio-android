object Libs {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:${Version.kotlinVersion}"

    const val appCompat = "androidx.appcompat:appcompat:${Version.appCompatVersion}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Version.constraintLayoutVersion}"
    const val material = "com.google.android.material:material:${Version.materialVersion}"
    const val core = "androidx.core:core-ktx:${Version.coreVersion}"

    // Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Version.retrofitVersion}"
    const val rxjava2 = "com.squareup.retrofit2:adapter-rxjava2:${Version.retrofitVersion}"

    // Gson
    const val gson = "com.google.code.gson:gson:${Version.retrofitVersion}"
    const val converterGson = "com.squareup.retrofit2:converter-gson:${Version.retrofitVersion}"

    // OkHttp
    const val okhttp = "com.squareup.okhttp3:okhttp:${Version.okhttpVersion}"
    const val okhttpLoggingInterceptor =
        "com.squareup.okhttp3:mockwebserver:${Version.okhttpVersion}"

    // Koin
    const val koinCore = "io.insert-koin:koin-core:${Version.koinVersion}"
    const val koinAndroid = "io.insert-koin:koin-android:${Version.koinVersion}"
    const val koinWorkManager = "io.insert-koin:koin-androidx-workmanager:${Version.koinVersion}"
    const val koinNavigation = "io.insert-koin:koin-androidx-navigation:${Version.koinVersion}"
    const val koinTest = "io.insert-koin:koin-test:${Version.koinVersion}"

    // Coroutines
    const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Version.coroutines}"

    // Lifecycle
    const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Version.lifecycle}"
    const val lifecycleLiveData = "androidx.lifecycle:lifecycle-livedata-ktx:${Version.lifecycle}"
    const val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-ktx:${Version.lifecycle}"

    // Room
    const val room = "androidx.room:room-runtime:${Version.room}"
    const val roomCoroutines = "androidx.room:room-ktx:${Version.room}"
    const val roomKart = "androidx.room:room-compiler:${Version.room}"

    // Picasso
    const val picasso = "com.squareup.picasso:picasso:${Version.picasso}"

    // Navigation
    const val navigationFragment =
        "androidx.navigation:navigation-fragment-ktx:${Version.navigation}"
    const val navigation = "androidx.navigation:navigation-ui-ktx:${Version.navigation}"
}