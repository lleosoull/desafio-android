object TestLibs {

    const val coreTest = "androidx.test:core-ktx:${Version.testRunner}"
    const val runner = "androidx.test:runner:${Version.testRunner}"

    // Coroutines
    const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Version.testCoroutines}"
    const val turbine = "app.cash.turbine:turbine:${Version.testTurbine}"

    // Mockk
    const val mockk = "io.mockk:mockk:${Version.testMockk}"
    const val mockkInstrumentation = "io.mockk:mockk-android:${Version.testMockk}"

    // Arch Core
    const val core = "androidx.arch.core:core-testing:${Version.testCore}"
    const val coreCommon = "androidx.arch.core:core-common:${Version.testCore}"
    const val coreRuntime = "androidx.arch.core:core-runtime:${Version.testCore}"

    // Koin
    const val koinJunit4 = "io.insert-koin:koin-test-junit4:${Version.koinVersion}"
    const val koinJunit5 = "io.insert-koin:koin-test-junit5:${Version.koinVersion}"

    // Espresso
    const val espressoCore = "androidx.test.espresso:espresso-core:${Version.testEspresso}"
    const val espressoIntents = "androidx.test.espresso:espresso-intents:${Version.testEspresso}"

    // Fragment
    const val fragment = "androidx.fragment:fragment-testing:${Version.testFragment}"

    // Junit
    const val junit  = "androidx.test.ext:junit:${Version.testJunit}"
}