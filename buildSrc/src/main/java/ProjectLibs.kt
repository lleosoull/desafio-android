object ProjectLibs {

    // Library
    const val netWork = ":library:network"
    const val exception = ":library:exception"
    const val database = ":library:database"

    // Feature
    const val list = ":features:list"

    // Design System
    const val designSystem = ":designsystem"
}