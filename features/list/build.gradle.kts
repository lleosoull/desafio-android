plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("org.jetbrains.kotlin.android.extensions")
    id("org.jetbrains.kotlin.kapt")
}

android {
    compileSdk = 31

    defaultConfig {
        minSdk = 21
        targetSdk = 31

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    buildFeatures {
        viewBinding = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(project(ProjectLibs.netWork))
    implementation(project(ProjectLibs.exception))
    implementation(project(ProjectLibs.designSystem))
    implementation(project(ProjectLibs.database))

    implementation(Libs.core)
    implementation(Libs.appCompat)
    implementation(Libs.material)
    implementation(Libs.constraintLayout)
    implementation(Libs.koinCore)
    implementation(Libs.koinAndroid)
    implementation(Libs.koinNavigation)
    implementation(Libs.koinWorkManager)
    implementation(Libs.retrofit)
    implementation(Libs.gson)
    implementation(Libs.coroutines)
    implementation(Libs.lifecycleLiveData)
    implementation(Libs.lifecycleRuntime)
    implementation(Libs.lifecycleViewModel)
    implementation(Libs.picasso)
    kapt(Libs.roomKart)

    testImplementation(TestLibs.turbine)
    testImplementation(TestLibs.coroutines)
    testImplementation(TestLibs.mockk)
    testImplementation(TestLibs.core)
    testImplementation(TestLibs.coreCommon)
    testImplementation(TestLibs.coreRuntime)
    testImplementation(TestLibs.koinJunit4)
    testImplementation(TestLibs.koinJunit5)

    androidTestImplementation(Libs.koinTest)
    androidTestImplementation(Libs.koinCore)
    androidTestImplementation(Libs.koinAndroid)
    androidTestImplementation(Libs.koinNavigation)
    androidTestImplementation(TestLibs.mockk)
    androidTestImplementation(TestLibs.espressoCore)
    androidTestImplementation(TestLibs.espressoIntents)
    androidTestImplementation(TestLibs.runner)
    androidTestImplementation(TestLibs.coreTest)
    androidTestImplementation(TestLibs.fragment)
    androidTestImplementation(TestLibs.junit)
}