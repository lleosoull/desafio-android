package com.challenge.list.presentation.list.robot

import androidx.test.espresso.matcher.ViewMatchers
import com.challenge.list.R
import com.challenge.list.domain.model.User
import com.challenge.list.domain.usecase.GetListUserUseCase
import com.challenge.list.presentation.list.ListViewModel
import io.mockk.coEvery
import kotlinx.coroutines.flow.flow
import org.koin.core.annotation.KoinExperimentalAPI
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

@OptIn(KoinExperimentalAPI::class)
internal fun onCreateFragment(func: ListFragmentRobot.() -> Unit): ListFragmentRobot {
    return ListFragmentRobot().apply(func)
}

@KoinExperimentalAPI
internal class ListFragmentRobot : KoinComponent {
    private val useCase: GetListUserUseCase by inject()
    private val viewModel = ListViewModel(getListUserUseCase = useCase)

    init {
        loadKoinModules(module(override = false) { single { viewModel } })
    }

    fun mockGetListUser() {
        coEvery { useCase.invoke() } returns flow { emit(stubsListUser()) }
    }

    infix fun check(func: ListFragmentRobot.() -> Unit): ListFragmentRobot {
        return ListFragmentRobot().apply(func)
    }

    fun displayList(text: String) {
        RecyclerViewMatchers.checkRecyclerViewItem(
            R.id.list_recycler_view,
            0,
            ViewMatchers.withText(text)
        )
    }
}

internal fun stubsListUser(): List<User> {
    return listOf(
        User(
            id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
            icon = "https://avatars.githubusercontent.com/u/82592?v=4",
            name = "Luffy D. Monkey",
            description = "Luffy D. Monkey",
            score = 5.0
        ),
        User(
            id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
            icon = "https://avatars.githubusercontent.com/u/82592?v=4",
            name = "Roronoa Zoro",
            description = "Roronoa Zoro",
            score = 5.0
        ),
        User(
            id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
            icon = "https://avatars.githubusercontent.com/u/82592?v=4",
            name = "Vinsmoke Sanji",
            description = "Vinsmoke Sanji",
            score = 5.0
        )
    )
}