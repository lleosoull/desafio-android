package com.challenge.list.presentation.list

import androidx.test.espresso.intent.Intents
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.challenge.di.registerDatabase
import com.challenge.list.di.registerListModule
import com.challenge.list.domain.usecase.GetListUserUseCase
import com.challenge.list.presentation.list.robot.onCreateFragment
import io.mockk.mockk
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.annotation.KoinExperimentalAPI
import org.koin.core.annotation.KoinInternalApi
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule

@KoinInternalApi
@OptIn(KoinExperimentalAPI::class)
@RunWith(AndroidJUnit4ClassRunner::class)
internal class ListFragmentTest : KoinTest {

    @get:Rule
    val koinModule = KoinTestRule.create {
        modules(
            registerListModule() + registerDatabase() + mockUseCase
        )
    }

    private val mockUseCase = module {
        single { mockk<GetListUserUseCase>(relaxed = true) }
    }

    @Before
    fun setup() = Intents.init()

    @After
    fun release() = Intents.release()

    @Test
    fun shouldDisplaySuccess() {
        onCreateFragment {
            mockGetListUser()
        } check {
            displayList("Luffy D. Monkey")
        }
    }
}