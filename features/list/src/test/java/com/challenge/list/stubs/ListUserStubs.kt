package com.challenge.list.stubs

import com.challenge.database.entity.UserEntity
import com.challenge.list.data.module.ItemResponse
import com.challenge.list.data.module.OwnerResponse
import com.challenge.list.data.module.UserResponse
import com.challenge.list.domain.model.User

internal fun stubsListUser(): List<User> {
    return listOf(
        User(
            id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
            icon = "https://avatars.githubusercontent.com/u/82592?v=4",
            name = "Luffy D. Monkey",
            description = "Luffy D. Monkey",
            score = 5.0
        ),
        User(
            id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
            icon = "https://avatars.githubusercontent.com/u/82592?v=4",
            name = "Roronoa Zoro",
            description = "Roronoa Zoro",
            score = 5.0
        ),
        User(
            id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
            icon = "https://avatars.githubusercontent.com/u/82592?v=4",
            name = "Vinsmoke Sanji",
            description = "Vinsmoke Sanji",
            score = 5.0
        )
    )
}

internal fun stubsListUserResponse(): UserResponse {
    return UserResponse(
        items = listOf(
            ItemResponse(
                id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
                owner = OwnerResponse(
                    icon = "https://avatars.githubusercontent.com/u/82592?v=4",
                ),
                name = "Luffy D. Monkey",
                description = "Luffy D. Monkey",
                score = 5.0
            ),
            ItemResponse(
                id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
                owner = OwnerResponse(
                    icon = "https://avatars.githubusercontent.com/u/82592?v=4",
                ),
                name = "Roronoa Zoro",
                description = "Roronoa Zoro",
                score = 5.0
            ),
            ItemResponse(
                id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
                owner = OwnerResponse(
                    icon = "https://avatars.githubusercontent.com/u/82592?v=4",
                ),
                name = "Vinsmoke Sanji",
                description = "Vinsmoke Sanji",
                score = 5.0
            )
        )
    )
}

internal fun stubsListUserEntity(): List<UserEntity> {
    return listOf(
        UserEntity(
            id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
            icon = "https://avatars.githubusercontent.com/u/82592?v=4",
            name = "Luffy D. Monkey",
            description = "Luffy D. Monkey",
            score = 5.0
        ),
        UserEntity(
            id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
            icon = "https://avatars.githubusercontent.com/u/82592?v=4",
            name = "Roronoa Zoro",
            description = "Roronoa Zoro",
            score = 5.0
        ),
        UserEntity(
            id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
            icon = "https://avatars.githubusercontent.com/u/82592?v=4",
            name = "Vinsmoke Sanji",
            description = "Vinsmoke Sanji",
            score = 5.0
        )
    )
}