package com.challenge.list.stubs

import com.challenge.list.presentation.list.ListViewState
import com.challenge.list.presentation.list.adapter.model.UserItem

internal fun stubsListViewStateSuccess(): ListViewState {
    return ListViewState(
        isLoading = false,
        listUser = listOf(
            UserItem(
                id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
                icon = "https://avatars.githubusercontent.com/u/82592?v=4",
                name = "Luffy D. Monkey",
                description = "Luffy D. Monkey",
                score = 5.0
            ),
            UserItem(
                id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
                icon = "https://avatars.githubusercontent.com/u/82592?v=4",
                name = "Roronoa Zoro",
                description = "Roronoa Zoro",
                score = 5.0
            ),
            UserItem(
                id = "MDEwOlJlcG9zaXRvcnk1MTUyMjg1",
                icon = "https://avatars.githubusercontent.com/u/82592?v=4",
                name = "Vinsmoke Sanji",
                description = "Vinsmoke Sanji",
                score = 5.0
            )
        )
    )
}