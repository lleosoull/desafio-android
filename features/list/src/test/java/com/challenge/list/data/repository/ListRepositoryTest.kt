package com.challenge.list.data.repository

import app.cash.turbine.test
import com.challenge.list.data.datasource.ListDataSource
import com.challenge.list.stubs.stubsListUser
import com.challenge.list.stubs.stubsListUserEntity
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.time.ExperimentalTime
import org.junit.Assert.assertEquals

@ExperimentalCoroutinesApi
@ExperimentalTime
class ListRepositoryTest {

    private val datasource: ListDataSource = mockk()
    private val repository = ListRepositoryImpl(datasource)

    @Test
    fun `when getListUser is called, state should success`() {
        // Given
        val listUserEntity = stubsListUserEntity()
        val expected = stubsListUser()
        coEvery { datasource.getListUser() } returns flow { emit(listUserEntity) }

        // When
        val result = repository.getListUser()

        // Then
        runBlocking {
            result.test {
                assertEquals(expected, expectItem())
                expectComplete()
            }
        }
    }
}