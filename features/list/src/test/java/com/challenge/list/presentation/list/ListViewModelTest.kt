package com.challenge.list.presentation.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.challenge.exceptionchallenge.ErrorException
import com.challenge.list.domain.usecase.GetListUserUseCase
import com.challenge.list.stubs.stubsListUser
import com.challenge.list.stubs.stubsListViewStateSuccess
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Rule
import org.junit.Before
import org.junit.After
import org.junit.Test
import org.junit.Assert.assertTrue
import org.junit.Assert.assertEquals

@ExperimentalCoroutinesApi
internal class ListViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()
    var testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun clearTests() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    private val useCase: GetListUserUseCase = mockk()
    private val viewModel = ListViewModel(
        getListUserUseCase = useCase,
    )

    @Test
    fun `when getListUser is called, state should error no connection`() {
        // Given
        val isLoadingState = ListViewState(isLoading = true)
        coEvery { useCase.invoke() } returns flow { throw ErrorException.NoConnectionException() }

        // When
        viewModel.getListUser()

        // Then
        assertEquals(viewModel.stateUi.value, isLoadingState)
        assertTrue(viewModel.actionUi.value is ListAction.NoConnectionException)
    }

    @Test
    fun `when getListUser is called, state should error generic`() {
        // Given
        val isLoadingState = ListViewState(isLoading = true)
        coEvery { useCase.invoke() } returns flow { throw ErrorException.InternalServerException() }

        // When
        viewModel.getListUser()

        // Then
        assertEquals(viewModel.stateUi.value, isLoadingState)
        assertTrue(viewModel.actionUi.value is ListAction.ErrorGeneric)
    }

    @Test
    fun `when getListUser is called, state should success`() {
        // Given
        val listUser = stubsListUser()
        val stateSuccess = stubsListViewStateSuccess()
        coEvery { useCase.invoke() } returns flow { emit(listUser) }

        // When
        viewModel.getListUser()

        // Then
        assertEquals(viewModel.stateUi.value, stateSuccess)
    }
}