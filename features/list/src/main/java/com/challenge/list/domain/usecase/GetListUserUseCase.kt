package com.challenge.list.domain.usecase

import com.challenge.list.domain.repository.ListRepository

internal class GetListUserUseCase(
    private val repository: ListRepository
) {
    operator fun invoke() = repository.getListUser()
}