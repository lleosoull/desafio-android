package com.challenge.list.data.api

import com.challenge.list.data.module.UserResponse
import retrofit2.http.GET

internal interface ListService {

    @GET("search/repositories?q=language:kotlin&sort=stars&page%20=1")
    suspend fun getListUser(): UserResponse
}