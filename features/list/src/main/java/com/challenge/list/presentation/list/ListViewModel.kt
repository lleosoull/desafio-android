package com.challenge.list.presentation.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge.exceptionchallenge.ErrorException
import com.challenge.list.domain.model.User
import com.challenge.list.domain.usecase.GetListUserUseCase
import com.challenge.list.presentation.list.adapter.mapper.userToUserItemMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

internal class ListViewModel(
    private val getListUserUseCase: GetListUserUseCase,
) : ViewModel() {

    private val _actionUi = MutableLiveData<ListAction>()
    private val _stateUi = MutableLiveData<ListViewState>()

    val actionUi: LiveData<ListAction>
        get() = _actionUi
    val stateUi: LiveData<ListViewState>
        get() = _stateUi

    init {
        getListUser()
    }

    fun getListUser() {
        viewModelScope.launch {
            getListUserUseCase()
                .flowOn(Dispatchers.IO)
                .onStart { _stateUi.postValue(ListViewState().enableLoading()) }
                .catch { setupError(it) }
                .collect { setupSuccess(it) }
        }
    }

    private fun setupError(throwable: Throwable) {
        when (throwable) {
            is ErrorException.NoConnectionException -> _actionUi.postValue(
                ListAction.NoConnectionException
            )
            else -> _actionUi.postValue(ListAction.ErrorGeneric)
        }
    }

    private fun setupSuccess(listUser: List<User>) {
        _stateUi.postValue(
            ListViewState(
                isLoading = false,
                listUser = userToUserItemMapper(listUser)
            )
        )
    }

}