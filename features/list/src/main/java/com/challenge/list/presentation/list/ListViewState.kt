package com.challenge.list.presentation.list

import com.challenge.list.presentation.list.adapter.model.UserItem

internal data class ListViewState(
    val isLoading: Boolean = false,
    val listUser: List<UserItem> = emptyList()
) {
    fun enableLoading(): ListViewState {
        return ListViewState(
            isLoading = true
        )
    }
}