package com.challenge.list.data.mapper

import com.challenge.database.entity.UserEntity
import com.challenge.list.data.module.UserResponse
import com.challenge.list.domain.model.User

internal fun userResponseToUserDaoMapper(userResponse: UserResponse): List<UserEntity> {
    return userResponse.items.map {
        UserEntity(
            id = it.id,
            name = it.name,
            description = it.description,
            score = it.score,
            icon = it.owner.icon,
        )
    }
}

internal fun userEntityToUserMapper(userEntity: List<UserEntity>): List<User> {
    return userEntity.map {
        User(
            id = it.id,
            name = it.name,
            description = it.description,
            score = it.score,
            icon = it.icon
        )
    }
}