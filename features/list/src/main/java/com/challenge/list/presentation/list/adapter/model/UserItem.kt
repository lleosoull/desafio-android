package com.challenge.list.presentation.list.adapter.model

internal data class UserItem(
    val id: String,
    val icon: String,
    val name: String,
    val description: String,
    val score: Double,
)