package com.challenge.list.presentation.list.adapter.holder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.challenge.list.databinding.ListUserItemBinding
import com.challenge.list.presentation.list.adapter.model.UserItem
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

internal class UserViewHolder(
    private val binding: ListUserItemBinding,
) : RecyclerView.ViewHolder(binding.root) {

    fun bindItem(item: UserItem) {
        binding.usernameTextViewUserItem.text = item.name
        binding.descriptionTextViewUserItem.text = item.description

        if (item.icon.isNotEmpty()) {
            Picasso.get()
                .load(item.icon)
                .error(com.challenge.ui.R.drawable.ic_round_account_circle)
                .into(binding.imageUserItem, object : Callback {
                    override fun onSuccess() {
                        binding.progressBar.isVisible = false
                    }

                    override fun onError(e: Exception?) {
                        binding.progressBar.isVisible = false
                    }
                })
        }
    }

    companion object {
        fun build(parent: ViewGroup): UserViewHolder {
            return UserViewHolder(
                ListUserItemBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
        }
    }
}