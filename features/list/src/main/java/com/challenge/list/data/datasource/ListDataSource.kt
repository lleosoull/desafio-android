package com.challenge.list.data.datasource

import com.challenge.database.dao.UserDao
import com.challenge.database.entity.UserEntity
import com.challenge.exceptionchallenge.parseException
import com.challenge.list.data.api.ListService
import com.challenge.list.data.mapper.userResponseToUserDaoMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

internal class ListDataSource(
    private val service: ListService,
    private val userDao: UserDao,
) {
    fun getListUser(): Flow<List<UserEntity>> {
        return flow {
            userDao.insertUserList(
                userResponseToUserDaoMapper(
                    service.getListUser()
                )
            )

            emit(userDao.getUserList())
        }.catch { throwable ->
            throw parseException(throwable)
        }
    }
}