package com.challenge.list.domain.repository

import com.challenge.list.domain.model.User
import kotlinx.coroutines.flow.Flow

internal interface ListRepository {
    fun getListUser(): Flow<List<User>>
}