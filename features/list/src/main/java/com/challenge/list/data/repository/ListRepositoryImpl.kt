package com.challenge.list.data.repository

import com.challenge.list.data.datasource.ListDataSource
import com.challenge.list.data.mapper.userEntityToUserMapper
import com.challenge.list.domain.model.User
import com.challenge.list.domain.repository.ListRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

internal class ListRepositoryImpl(
    private val dataSource: ListDataSource,
) : ListRepository {
    override fun getListUser(): Flow<List<User>> {
        return dataSource.getListUser().map { userEntityToUserMapper(it) }
    }
}