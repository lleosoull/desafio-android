package com.challenge.list.di

import com.challenge.list.data.api.ListService
import com.challenge.list.data.datasource.ListDataSource
import com.challenge.list.data.repository.ListRepositoryImpl
import com.challenge.list.domain.repository.ListRepository
import com.challenge.list.domain.usecase.GetListUserUseCase
import com.challenge.list.presentation.list.ListViewModel
import com.challenge.network.createServiceApi
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModel

private const val URL = "https://api.github.com/"

fun registerListModule(): List<Module> {
    return listOf(
        dataModulo,
        domainModulo,
        presentationModule,
    )
}

private val dataModulo = module {
    factory {
        createServiceApi<ListService>(
            url = URL,
        )
    }
    single { ListDataSource(service = get(), userDao = get()) }
}

private val domainModulo = module {
    single<ListRepository> { ListRepositoryImpl(dataSource = get()) }
    single { GetListUserUseCase(repository = get()) }
}

private val presentationModule = module {
    viewModel {
        ListViewModel(
            getListUserUseCase = get(),
        )
    }
}