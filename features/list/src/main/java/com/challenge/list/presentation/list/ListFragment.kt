package com.challenge.list.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.challenge.list.databinding.ListUserFragmentBinding
import com.challenge.list.presentation.list.adapter.UserListAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListFragment : Fragment() {

    private val viewModel by viewModel<ListViewModel>()
    private val userAdapter: UserListAdapter by lazy { UserListAdapter() }
    private val manager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

    private lateinit var binding: ListUserFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ListUserFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        setupObserversAction()
        setupObserversState()
    }

    private fun setupAdapter() {
        with(binding.listRecyclerView) {
            layoutManager = manager
            adapter = userAdapter
        }
    }

    private fun setupObserversAction() {
        viewModel.actionUi.observe(viewLifecycleOwner) { action ->
            when (action) {
                is ListAction.NoConnectionException -> {
                    //TODO adicionar dialog
                }
                is ListAction.ErrorGeneric -> {
                    //TODO adicionar dialog
                }
            }
        }
    }

    private fun setupObserversState() {
        viewModel.stateUi.observe(viewLifecycleOwner) { state ->
            if (state.listUser.isNotEmpty()) {
                userAdapter.submitList(state.listUser)
            }
        }
    }
}