package com.challenge.list.data.module

import com.google.gson.annotations.SerializedName

internal data class UserResponse(
    val items: List<ItemResponse>
)

internal data class ItemResponse(
    @SerializedName("node_id")
    val id: String,
    val name: String,
    val description: String,
    val score: Double,
    val owner: OwnerResponse,
)

internal data class OwnerResponse(
    @SerializedName("avatar_url")
    val icon: String,
)