package com.challenge.list.presentation.list.adapter.mapper

import com.challenge.list.domain.model.User
import com.challenge.list.presentation.list.adapter.model.UserItem

internal fun userToUserItemMapper(userList: List<User>): List<UserItem> {
    return userList.map {
        UserItem(
            id = it.id,
            icon = it.icon,
            name = it.name,
            description = it.description,
            score = it.score,
        )
    }
}