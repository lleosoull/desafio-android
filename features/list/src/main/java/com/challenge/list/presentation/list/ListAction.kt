package com.challenge.list.presentation.list

internal sealed class ListAction {
    object NoConnectionException : ListAction()
    object ErrorGeneric : ListAction()
}