package com.challenge.list.domain.model

internal data class User(
    val id: String,
    val icon: String,
    val name: String,
    val description: String,
    val score: Double,
)