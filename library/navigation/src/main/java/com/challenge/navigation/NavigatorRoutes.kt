package com.challenge.navigation

import android.net.Uri
import android.os.Bundle
import androidx.navigation.NavController

object NavigatorRoutes {

    fun gotToDetailUser(navController: NavController, bundle: Bundle? = null) {
        val deepLinkUri = Uri.parse("app://challenge/user_detail_fragment")
        val navDestination = navController.graph.find {
            it.hasDeepLink(deepLinkUri)
        }
        navDestination?.let {
            navController.navigate(navDestination.id, bundle)
        }
    }
}