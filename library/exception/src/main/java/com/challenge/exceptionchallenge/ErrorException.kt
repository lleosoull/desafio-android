package com.challenge.exceptionchallenge

sealed class ErrorException : Exception() {
    data class NoConnectionException(
        override val message: String? = null
    ) : ErrorException()

    data class InternalServerException(
        override val message: String? = null
    ) : ErrorException()
}