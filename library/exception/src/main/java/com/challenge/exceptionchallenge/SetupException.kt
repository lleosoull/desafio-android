package com.challenge.exceptionchallenge

import java.net.ConnectException
import java.net.UnknownHostException

fun parseException(throwable: Throwable): ErrorException {
    return when (throwable) {
        is UnknownHostException,
        is ConnectException -> ErrorException.NoConnectionException()
        else -> ErrorException.InternalServerException()
    }
}