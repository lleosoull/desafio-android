package com.challenge.di

import com.challenge.database.MainRoomDatabase
import org.koin.core.module.Module
import org.koin.dsl.module

fun registerDatabase(): List<Module> {
    return listOf(setupModule)
}

private val setupModule = module {
    factory { MainRoomDatabase.getInstance(context = get()) }
    factory { get<MainRoomDatabase>().userDao() }
}