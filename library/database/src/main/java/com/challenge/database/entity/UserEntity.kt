package com.challenge.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class UserEntity(
    @PrimaryKey
    val id: String,
    val icon: String,
    val name: String,
    val description: String,
    val score: Double,
)