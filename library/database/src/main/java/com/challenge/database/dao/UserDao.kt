package com.challenge.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.challenge.database.entity.UserEntity

@Dao
interface UserDao {

    @Query("SELECT * from user_table")
    suspend fun getUserList(): List<UserEntity>

    @Query("DELETE from user_table")
    fun deleteUserTable()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserList(userList: List<UserEntity>)
}