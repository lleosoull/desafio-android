package com.challenge.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.challenge.database.dao.UserDao
import com.challenge.database.entity.UserEntity

@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
internal abstract class MainRoomDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var INSTANCE: MainRoomDatabase? = null

        fun getInstance(context: Context): MainRoomDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            MainRoomDatabase::class.java, "user_database"
        ).build()
    }
}